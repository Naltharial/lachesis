import abc


class TokenError(ValueError):
    def __init__(self, token, message):
        super(TokenError, self).__init__(message)

        self.token = token
        self.message = message


class Expression(object, metaclass=abc.ABCMeta):
    @abc.abstractmethod
    def result(self, resolver):
        raise NotImplementedError("Implement a result method")


class CmdExpression(Expression):
    def __init__(self, cmd, expr, arglist=None):
        self.cmd = cmd
        self.expr = expr
        self.args = arglist

    def __repr__(self):
        return "Cmd: [{0!s} {1!s} {2!s}]".format(self.cmd, self.expr, self.args)

    def result(self, resolver):
        return self.expr.result(resolver)


class ListExpression(Expression):
    def __init__(self, exprlist=tuple(), expr=None):
        if isinstance(exprlist, ListExpression):
            exprlist = exprlist.exprlist
        if expr:
            exprlist += expr,

        self.exprlist = exprlist

    def __repr__(self):
        return "Exprs: [{0!s}]".format(self.exprlist)

    def result(self, resolver):
        return (e.result(resolver) for e in self.exprlist)


class ConstExpression(Expression):
    def __init__(self, const):
        self.const = const

    def __repr__(self):
        return "Const: [{0!s}]".format(self.const)

    def result(self, resolver):
        return self.const


class NameExpression(Expression):
    def __init__(self, name):
        self.name = name

    def __repr__(self):
        return "Name: [{0!s}]".format(self.name)

    def result(self, resolver):
        result = resolver(self.name)
        if result is None:
            raise TokenError(self.name, "not recognized!")

        return result


class BinExpression(Expression):
    def __init__(self, op, left, right):
        self.op = op
        self.left = left
        self.right = right

    def __repr__(self):
        return "Bin: [{0!s} {1!s} {2!s}]".format(self.left, self.op, self.right)

    def result(self, resolver):
        l = self.left.result(resolver)
        r = self.right.result(resolver)

        if self.op == 'PLUS':
            return l + r
        if self.op == 'MINUS':
            return l - r
        if self.op == 'TIMES':
            return l * r
        if self.op == 'DIVIDE':
            return int(l / r)

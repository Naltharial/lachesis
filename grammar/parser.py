from sly import Parser as SlyParser

from grammar import expression
from grammar.lexer import Lexer


class Parser(SlyParser):
    tokens = Lexer.tokens

    @_('CMD NAME exprlist')
    def cmd(self, p):
        return expression.CmdExpression(p.NAME, p.exprlist)

    @_('CMD NAME exprlist arglist')
    def cmd(self, p):
        return expression.CmdExpression(p.NAME, p.exprlist, p.arglist)

    @_('')
    def arglist(self, p):
        return expression.ListExpression()

    @_('arglist arg')
    def arglist(self, p):
        return expression.ListExpression(p.arglist, p.arg)

    # FIXME: reduce/reduce conflict

    @_('CHANNEL')
    def arg(self, p):
        return p.CHANNEL

    @_('USER')
    def arg(self, p):
        return p.USER

    @_('COMMENT')
    def arg(self, p):
        return p.COMMENT

    @_('')
    def exprlist(self, p):
        return expression.ListExpression()

    @_('exprlist expr')
    def exprlist(self, p):
        return expression.ListExpression(p.exprlist, p.expr)

    @_('expr PLUS term')
    def expr(self, p):
        return expression.BinExpression('PLUS', p.expr, p.term)

    @_('expr MINUS term')
    def expr(self, p):
        return expression.BinExpression('MINUS', p.expr, p.term)

    @_('term')
    def expr(self, p):
        return p.term

    @_('term TIMES factor')
    def term(self, p):
        return expression.BinExpression('TIMES', p.term, p.factor)

    @_('term DIVIDE factor')
    def term(self, p):
        return expression.BinExpression('DIVIDE', p.term, p.factor)

    @_('factor')
    def term(self, p):
        return p.factor

    @_('NAME')
    def factor(self, p):
        return expression.NameExpression(p.NAME)

    @_('NUMBER')
    def factor(self, p):
        return expression.ConstExpression(p.NUMBER)

    @_('LPAREN expr RPAREN')
    def factor(self, p):
        return p.expr

from sly import Lexer as SlyLexer


class Lexer(SlyLexer):
    tokens = {'CMD',
              'NAME', 'NUMBER', 'PLUS', 'TIMES', 'MINUS', 'DIVIDE',
              'LPAREN', 'RPAREN',
              'CHANNEL', 'USER', 'COMMENT'}

    ignore = ' \t'

    CMD = r'!'
    NAME = r'[a-zA-Z_][a-zA-Z0-9_]*'
    PLUS = r'\+'
    MINUS = r'-'
    TIMES = r'\*'
    DIVIDE = r'/'
    LPAREN = r'\('
    RPAREN = r'\)'
    CHANNEL = r'\@\S+'
    USER = r'\?\S+'
    COMMENT = r'\#.+'

    @_(r'\d+')
    def NUMBER(self, t):
        t.value = int(t.value)
        return t

    def error(self, t):
        print("Illegal character '%s'" % t.value[0])
        self.index += 1

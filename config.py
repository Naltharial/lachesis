import json


class Config(object):
    def __init__(self, filename):
        with open(filename, 'r') as fp:
            self._config = json.load(fp)

        self.ID = self._config['id']
        self.TOKEN = self._config['token']

        self.MODULES = self._config['modules']

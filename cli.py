import pprint
import pathlib

from playhouse.shortcuts import model_to_dict

from modules.intel.db import sqlite_db, Station
from modules.intel.parsers import TianFullParser, TianPartialParser, FullParser, PartialParser, NoParser
from modules.intel.embed import StationEmbed

sqlite_db.connect()
sqlite_db.create_tables([Station])


def _make_parser(content: str):
    if 'Cards' in content:
        if 'unknown' in content:
            parser = TianPartialParser(content, "Naltharial#0001", 1234567890)
        else:
            parser = TianFullParser(content, "Naltharial#0001", 1234567890)
    else:
        if 'unable to gather any intel' in content:
            parser = NoParser(content, "Naltharial#0001", 1234567890)
        elif 'unknown' in content:
            parser = PartialParser(content, "Naltharial#0001", 1234567890)
        else:
            parser = FullParser(content, "Naltharial#0001", 1234567890)

    return parser


if __name__ == "__main__":
    tests = [
        'tian_test_intel_2.txt',
        'tian_test_intel.txt',
        'test_intel.txt',
        'full_intel.txt',
        'tian_full_intel.txt',
        'tian_partial_intel.txt'
    ]
    for filename in tests:
        content = open(pathlib.Path(__file__).parent / '..' / 'test3_env' / 'dnd' / filename, 'rt',
                       encoding='utf-8').read()
        parser = _make_parser(content)
        parser.parse()
        if parser.Meta.save:
            ret = Station.replace(model_to_dict(parser.station)).execute()
            print(ret)

        existing = Station.latest(1234567890, parser.station.hex_x, parser.station.hex_y)
        embed = StationEmbed(existing)
        pprint.pprint(embed.get().to_dict())

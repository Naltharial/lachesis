import logging

from modules.dice.rolls.roll import Roll

logger = logging.getLogger(__name__)


class TextFormatter(object):
    @classmethod
    def keycap(cls, i):
        kc = "{i}\U000020E3".format(i=i)
        if i == 10:
            kc = "\U0001F51F"

        return kc

    @classmethod
    def reply_raw(cls, user: str, result: Roll, comment=None):
        reply_roll = "`{dice}`".format(dice=" ".join(str(i) for i in result))
        if comment:
            reply_roll += " ({comment})".format(comment=comment)

        return "{name}: {result}".format(name=user, result=reply_roll)

    @classmethod
    def reply_roll(cls, user: str, result: Roll,
                   comment=None, keycap=False, short=False):
        mark = "\U0001F49A"
        base = result.successes + result.threshold
        if base < 0:
            mark = "\U0001F5A4"
        elif result.successes < 1:
            mark = "\U0001F49B"
        elif result.critical:
            mark = "\U0001F496"

        tstr = ""
        if result.threshold:
            tstr = "Threshold: [ **{ts}** ]".format(ts=result.threshold)
        elif result.extra:
            tstr = "Extra dies: [ **{ts}** ]".format(ts=result.extra)

        dstr = len(result)
        if result.extra:
            dstr = "{base} + {extra} = {result}".format(
                base=len(result)-result.extra,
                extra=result.extra,
                result=len(result)
            )

        sstr = ""
        if result.sides != 10:
            sstr = "**D**{sides}".format(sides=result.sides)
        reply_start = "Rolling [ **{dies}{sides}** ] dice against difficulty [ **{diff}** ] for {user}".format(
            dies=dstr,
            sides=sstr,
            diff=result.diff,
            user=user
        )
        if short:
            reply_start = "Rolling [ **{dies}** ] dice for {user}".format(
                dies=dstr,
                user=user
            )
        if comment:
            reply_start += " ({comment})".format(comment=comment)

        if keycap:
            reply_roll = " ".join(cls.keycap(i) for i in result)
        else:
            reply_roll = "`{dice}`".format(dice=" ".join(str(i) for i in result))

        sstr = str(result.successes)
        if result.threshold or result.botches:
            sstr = "{base} - {threshold} = {result}".format(
                base=result.threshold+result.botches+result.successes,
                threshold=result.threshold+result.botches,
                result=str(result.successes)
            )
        reply_result = "{mark}    {threshold} Successes: [ **{success}** ], Total: [ **{total}** ]".format(
            mark=mark,
            threshold=tstr,
            success=sstr,
            total=result.total
        )
        if short:
            reply_result = "Total: [ **{total}** ]".format(
                total=result.total
            )

        return reply_start + "\r\n" + reply_roll + "\r\n" + reply_result

    @classmethod
    async def reply_table(cls, table):
        separator = "+--------------------+------------+------------+"
        line = "| {f1:18s} | {f2:10s} | {f3:10s} |"

        message = [
            separator,
            line.format(f1=table[0][0], f2=table[0][1], f3=table[0][2]),
            separator,
            *[line.format(f1=str(row[0]), f2=str(row[1]), f3=(str(row[2]) or '')) for row in table[1:]],
            separator
        ]

        return '```' + '\r\n'.join(message) + '```'

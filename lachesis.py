import logging

from discord import Client, Game, Intents
from discord.ext.commands import Bot

from config import Config
from modules.admin.adminbot import Adminbot
from modules.claim.claimbot import Claimbot
from modules.dice.dicebot import Dicebot
from modules.intel.intelbot import Intelbot

logger = logging.getLogger(__name__)


class Lachesis(Bot):
    BOT_PREFIX = ("!",)
    config = Config("config.json")

    def __init__(self):
        super(Lachesis, self).__init__(command_prefix=self.BOT_PREFIX, intents=Intents.all())

    async def start(self, token: str, *, reconnect: bool = True) -> None:
        await self.add_cog(Adminbot(self, self.config))
        await self.add_cog(Claimbot(self, self.config))
        await self.add_cog(Dicebot(self, self.config))
        await self.add_cog(Intelbot(self, self.config))

        await super(Lachesis, self).start(token=token, reconnect=reconnect)

    async def on_ready(self):
        await self.change_presence(activity=Game(name="with your fate"))
        logger.info("Logged in as " + self.user.name)

    def run(self, **kwargs):
        if "token" not in kwargs:
            kwargs["token"] = self.config.TOKEN
        super(Lachesis, self).run(**kwargs)


if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO)

    Lachesis().run()

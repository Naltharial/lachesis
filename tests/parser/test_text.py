from modules.dice.parameters import Parameters


def test_parse_command():
    text = '!roll 1 10 6'
    params = Parameters(text)

    assert params.dies == 1
    assert params.sides == 10
    assert params.diff == 6


def test_parse_arglist():
    text = '!roll 1 10 6 @channel-name ?user #comment'
    params = Parameters(text)

    assert params.dies == 1
    assert params.sides == 10
    assert params.diff == 6

    assert params.comment == 'comment'
    assert params.channel == 'channel-name'
    assert params.user == 'user'

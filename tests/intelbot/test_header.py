from modules.intel.parsers import FullParser


def test_parse_header():
    parser = FullParser('', None, None)
    parser._header('Spy Report on hex (-287,29) Combine Production Plant completed 14 days, 5 hours and 17 minutes ago.')
    assert parser.station.name == 'Combine Production Plant'
    assert parser.station.tag is None
    assert parser.station.hex_x == -287
    assert parser.station.hex_y == 29

    parser._header('Spy Report on hex (99,119) The Great Forge  [SM] completed 0 seconds ago.')
    assert parser.station.name == 'The Great Forge'
    assert parser.station.tag == '[SM]'
    assert parser.station.hex_x == 99
    assert parser.station.hex_y == 119

    parser._header('Spy Report on hex (-215,101) P.I.T.A  [SOL] completed 1 minutes ago.')
    assert parser.station.name == 'P.I.T.A'
    assert parser.station.tag == '[SOL]'
    assert parser.station.hex_x == -215
    assert parser.station.hex_y == 101

    parser._header('Spy Report on hex (-173,17) MPL Mining Hub completed 1 seconds ago.')
    assert parser.station.name == 'MPL Mining Hub'
    assert parser.station.tag is None
    assert parser.station.hex_x == -173
    assert parser.station.hex_y == 17

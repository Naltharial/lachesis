import io
import re
import csv
import time
import logging

import arrow
import discord
from discord.ext.commands import Context, command

from modules.base import BaseModule

logger = logging.getLogger("adminbot")


class Adminbot(BaseModule):
    @command(help="Returns a CSV file of all users on the server")
    async def stat(self, context: Context, *args):
        if not self.valid_call(context):
            return

        criteria = args[0] if args else None

        before = time.time()
        members = []
        for m in context.guild.members:
            data = [m.display_name, m.name, arrow.get(m.joined_at).format('YYYY-MM-DD_HH-mm'), '']

            add = not bool(criteria)
            for r in m.roles:
                if r.name == '@everyone':
                    continue

                if not add and str(r.id) == criteria:
                    add = True

                data.append(r.name)

            if add:
                members.append(data)

        if members:
            f = io.StringIO()
            writer = csv.writer(f, dialect='excel-tab')
            for v in members:
                writer.writerow(v)
            f.seek(0)

            after = time.time()
            captured_at = arrow.utcnow().format('YYYY-MM-DD_HH-mm')
            name = re.sub(r'\W+|^(?=\d)','_', context.guild.name)
            raw_file = discord.File(f, filename=f"{name}_{captured_at}.csv")
            gen_took = int((after - before) * 1000)

            await context.channel.send(f"Member list generated in {str(gen_took)}ms.", file=raw_file)
        else:
            await context.channel.send(f"No members found matching role ID {criteria}.")

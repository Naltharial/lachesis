import logging

from discord.ext.commands import Context, command

from modules import hex_map
from modules.base import BaseModule
from modules.claim.db import sqlite_db, Claim

logger = logging.getLogger(__name__)

HEX_DISTANCE = 20

sqlite_db.connect()
sqlite_db.create_tables([Claim])


class Claimbot(BaseModule):
    @command(help="Records a hex claim")
    async def claim(self, context: Context,
                    *, hexes: str):
        if not self.valid_call(context):
            return

        parsed, comment = hex_map.parse_hexes(hexes)
        if len(parsed) != 1:
            await context.channel.send("Invalid hex specifier for a single hex")
            return

        hex_x, hex_y = parsed.pop()

        existing = Claim.get_or_none(
            Claim.user == context.author,
            Claim.channel == context.channel.id
        )

        create = False
        if not existing:
            warnings = Claim.hex_conflicts(context.channel.id, hex_x, hex_y, distance=HEX_DISTANCE)
            conflicts = Claim.hex_conflicts(context.channel.id, hex_x, hex_y, distance=HEX_DISTANCE/2)

            if not warnings:
                create = True
                message = f"Successfully registered `{hex_x} x {hex_y}` for {context.author.display_name}"
            else:
                if conflicts:
                    message = f"There are {len(conflicts)} claims in {10} hexes around `{hex_x} x {hex_y}`:"
                    for conflict in conflicts:
                        dist = conflict.distance(hex_x, hex_y)
                        message += f"\r\n  {conflict.user} - {conflict.hex_x} x {conflict.hex_y} ({dist} hexes)"
                else:
                    create = True
                    message = f"WARNING - there are {len(warnings)} claims in {20} hexes around `{hex_x} x {hex_y}`:"
                    for conflict in warnings:
                        dist = conflict.distance(hex_x, hex_y)
                        user = conflict.mention or conflict.display_name or conflict.user
                        message += f"\r\n  {user} - {conflict.hex_x} x {conflict.hex_y} ({dist} hexes)"

                    message += f"\r\n\r\nSuccessfully registered `{hex_x} x {hex_y}` for {context.author.display_name}"
        else:
            message = f"Claim already exists at `{existing.hex_x} x {existing.hex_y}` for {context.author.display_name} - use !clear"

        if create:
            new_claim = Claim(
                user=context.author,
                display_name=context.author.display_name,
                mention=context.author.mention,

                channel=context.channel.id,

                hex_x=hex_x,
                hex_y=hex_y
            )
            new_claim.save(force_insert=True)

        await self._reply(context, message)

    @command(help="Clears a hex claim")
    async def clear(self, context: Context):
        if not self.valid_call(context):
            return

        existing = Claim.get_or_none(
            Claim.user == context.author,
            Claim.channel == context.channel.id
        )

        if not existing:
            message = f"No claim found for {context.author.display_name}"
        else:
            existing.delete_instance()
            message = f"Claim at `{existing.hex_x} x {existing.hex_y}` cleared for {context.author.display_name}"

        await self._reply(context, message)

    @command(help="Checks a hex claim")
    async def check(self, context: Context,
                    *, hexes: str):
        if not self.valid_call(context):
            return

        parsed, comment = hex_map.parse_hexes(hexes)
        if len(parsed) != 1:
            await context.channel.send("Invalid hex specifier for a single hex")
            return

        hex_x, hex_y = parsed.pop()

        conflicts = Claim.hex_conflicts(context.channel.id, hex_x,  hex_y)
        if not conflicts:
            message = f"There are no claims in 20 hexes around `{hex_x} x {hex_y}`"
        else:
            message = f"There are {len(conflicts)} claims in {HEX_DISTANCE} hexes around `{hex_x} x {hex_y}`:"
            for conflict in conflicts:
                dist = conflict.distance(hex_x, hex_y)
                message += f"\r\n  {conflict.user} - {conflict.hex_x} x {conflict.hex_y} ({dist} hexes)"

        await self._reply(context, message)

    @command(help="Lists hex claims")
    async def list(self, context: Context):
        if not self.valid_call(context):
            return

        existing = Claim.select().where(
            Claim.channel == context.channel.id
        ).order_by(Claim.hex_x, Claim.hex_y)

        if not existing:
            message = f"No claims recorded"
        else:
            messages = []
            for hex_claim in existing:
                display = hex_claim.display_name or hex_claim.user
                messages.append(f"{hex_claim.hex_x:>4} x {hex_claim.hex_y:>4} :: {display}")

            message = '```' + '\r\n'.join(messages) + '```'

        await self._reply(context, message)

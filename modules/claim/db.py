import datetime

from peewee import *

from modules import hex_map


sqlite_db = SqliteDatabase('claimbot.db')


class BaseModel(Model):
    class Meta:
        database = sqlite_db


class Claim(BaseModel):
    class Meta:
        primary_key = CompositeKey('channel', 'hex_x', 'hex_y')

    user = TextField()
    display_name = TextField()
    mention = TextField()

    channel = IntegerField()

    hex_x = IntegerField()
    hex_y = IntegerField()

    claimed_at = DateTimeField(default=datetime.datetime.now)

    @classmethod
    def hex_conflicts(cls, channel_id, hex_x, hex_y, distance=20):
        existing = cls.select().where(cls.channel == channel_id)

        conflicts = []
        for hex_claim in existing:
            if hex_map.hex_distance(hex_x, hex_y, hex_claim.hex_x, hex_claim.hex_y) <= distance:
                conflicts.append(hex_claim)

        return conflicts

    def conflicts(self, distance=20):
        return self.hex_conflicts(self.channel, self.hex_x, self.hex_y, distance=distance)

    def distance(self, hex_x, hex_y):
        return hex_map.hex_distance(self.hex_x, self.hex_y, hex_x, hex_y)

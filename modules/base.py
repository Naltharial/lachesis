from typing import List, Dict

import discord
from discord.ext.commands import Bot, Cog


class BaseModule(Cog):
    bot: Bot
    config: Dict

    _lookup = {}

    def __init__(self, bot: Bot, config):
        self.bot = bot
        self.config = config.MODULES.get(self.__class__.__name__.lower(), {})

    def get_guild(self, guild_id: int):
        if guild_id not in self._lookup:
            self._lookup[guild_id] = self.bot.get_guild(guild_id)

        return self._lookup[guild_id]

    def get_user_channels(self, user: discord.User) -> List[int]:
        """Check common guilds to see what channel the user is requesting."""
        result = []
        for gid, confs in self.config.items():
            guild = self.get_guild(int(gid))
            if not guild:
                continue

            for conf in confs:
                gm = guild.get_member(user.id)
                if gm:
                    if 'roles' in conf:
                        for role in gm.roles:
                            if role.name in conf['roles']:
                                result.append(conf['channel'])
                    else:
                        result.append(conf['channel'])

        print(f"Returning channels: {result}")
        return result

    def valid_call(self, context, private=False):
        if private and context.channel.type == discord.ChannelType.private:
            return True

        if not context.guild:
            return False

        gid = str(context.guild.id)
        if gid not in self.config:
            return False

        confs = self.config[gid]
        for conf in confs:
            if 'channel' in conf:
                if conf['channel'] != str(context.channel.id):
                    continue

            if 'roles' in conf:
                for role in context.author.roles:
                    if role.name in conf['roles']:
                        return True
            else:
                return True

        return False

    @classmethod
    async def _reply(cls, context, message):
        await context.channel.send(message)

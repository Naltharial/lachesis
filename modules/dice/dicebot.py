import datetime
import logging
import numpy

import discord
from discord.ext.commands import Context, Cog, command

from modules.base import BaseModule
from grammar.expression import TokenError
from modules.dice.parameters import Parameters
from formatting.text import TextFormatter
from modules.dice.rolls.manager import RollManager
from modules.dice.sheets.store import Sheets

logger = logging.getLogger(__name__)


class Dicebot(BaseModule):
    def __init__(self, bot, config):
        super(Dicebot, self).__init__(bot, config)

        self.sheets = Sheets(config.MODULES.get('dicebot', {}))
        self.roll_manager = RollManager()

    @classmethod
    async def _reply(cls, context, message):
        await context.channel.send(message)

    def valid_call(self, context, private=False):
        if private and context.channel.type != discord.ChannelType.private:
            return False

        return True

    @command(help="Rolls the specified number of n-sided dice, as requested,"
                  " and calculates the sum and successes,"
                  " against an optional difficulty (with a default of 6).",
             brief="Rolls dice")
    async def roll(self, context: Context):
        if not self.valid_call(context):
            return

        try:
            params = Parameters(context.message.content)

            user = params.user or context.message.author.display_name
            channel = self.sheets.resolve_channel_id(params.channel) or str(context.channel.id)

            params.resolver = self.sheets.resolver(channel, user)

            dies = params.dies
            sides = params.sides or 10
            diff = params.diff or 6
        except TokenError as e:
            return await self._reply(context, "`{name}` {message}".format(name=e.token, message=e.message))
        except IndexError:
            return await self._reply(context, "No arguments supplied!")
        except ValueError as e:
            logger.warning(str(e))
            return

        result = self.roll_manager.roll(channel, user, 'roll', dies, sides, diff)

        message = TextFormatter.reply_roll(
            str(context.message.author.mention),
            result=result,
            comment=params.comment,
            short=False if params.diff is not None else True
        )

        await self._reply(context, message)

    @command(help="Rolls the specified number of n-sided dice, presented as unformatted output.",
             brief="Rolls dice with unformatted output")
    async def raw(self, context: Context):
        if not self.valid_call(context):
            return

        try:
            params = Parameters(context.message.content)

            user = params.user or context.message.author.display_name
            channel = self.sheets.resolve_channel_id(params.channel) or str(context.channel.id)

            params.resolver = self.sheets.resolver(channel, user)

            dies = params.dies
            sides = params.sides or 10
        except TokenError as e:
            return await self._reply(context, "`{name}` {message}".format(name=e.token, message=e.message))
        except IndexError:
            return await self._reply(context, "No arguments supplied!")
        except ValueError as e:
            logger.warning(str(e))
            return

        result = self.roll_manager.roll(channel, user, 'roll', dies, sides, sorting=False)
        if sides == 10:
            result.bounds = self.sheets.default_config(channel)['bounds']

        message = TextFormatter.reply_raw(
            user=str(context.message.author.mention),
            result=result,
            comment=params.comment
        )

        await self._reply(context, message)

    @command(help="Tests a roll against 1000 iterations.",
             brief="Test dice rolls")
    async def test(self, context: Context):
        if not self.valid_call(context):
            return

        try:
            params = Parameters(context.message.content)

            dies = params.dies
            sides = params.sides or 10
            diff = params.diff or 6

            user = params.user or context.message.author.display_name
            channel = self.sheets.resolve_channel_id(params.channel) or str(context.channel.id)
        except TokenError as e:
            return await self._reply(context, "`{name}` {message}".format(name=e.token, message=e.message))
        except IndexError:
            return await self._reply(context, "No arguments supplied!")
        except ValueError as e:
            logger.warning(str(e))
            return

        results = []
        for i in range(1000):
            result = self.roll_manager.roll(channel, user, 'roll', dies, sides, diff)
            if sides == 10:
                result.bounds = self.sheets.default_config(channel)['bounds']
            results.append(result)

        await self._reply(context, "{name}: {dice} x1000 - Mean: {mean:.2f}, Std: {std:.2f}".format(
            name=str(context.message.author.mention),
            dice="{}d{}diff{}thresh{}".format(results[0].dies, results[0].sides, results[0].diff, results[0].threshold),
            mean=numpy.mean(results),
            std=numpy.std(results)
        ))

    @command(help="Rolls the specified number of 10-sided dice, as requested,"
                  " and calculates the sum and successes (with botches),"
                  " against an optional difficulty (with a default of 6).",
             brief="Rolls d10 dice",
             aliases=('d',))
    async def d10(self, context: Context):
        if not self.valid_call(context):
            return

        try:
            params = Parameters(context.message.content)

            user = params.user or context.message.author.display_name
            channel = self.sheets.resolve_channel_id(params.channel) or str(context.channel.id)

            params.resolver = self.sheets.resolver(channel, user)

            dies = params.dies
            sides = 10
            diff = params.sides or 6
        except TypeError:
            return await self._reply(context, "Only number rolls supported on this channel.")
        except TokenError as e:
            return await self._reply(context, "`{name}` {message}".format(name=e.token, message=e.message))
        except IndexError:
            return await self._reply(context, "No arguments supplied!")
        except ValueError as e:
            logger.warning(str(e))
            return

        result = self.roll_manager.roll(channel, user, 'roll', dies, sides, diff)
        result.bounds = self.sheets.default_config(channel)['bounds']

        message = TextFormatter.reply_roll(
            str(context.message.author.mention),
            result=result,
            comment=params.comment,
            keycap=True
        )

        await self._reply(context, message)

    @command(help="Rolls the character initiative.",
             brief="Rolls initiative.",
             aliases=('i',))
    async def initiative(self, context: Context):
        if not self.valid_call(context):
            return

        try:
            mod = None
            comment = ''
            params = Parameters(context.message.content)

            user = params.user or context.message.author.display_name
            channel = self.sheets.resolve_channel_id(params.channel) or str(context.channel.id)

            params.resolver = self.sheets.resolver(channel, user)

            if params.comment:
                comment = " ({comment})".format(comment=params.comment)
            try:
                mod = params.dies
            except IndexError:
                pass

            if mod is None:
                mod = params.resolver("dexterity") + params.resolver("wit")
        except TypeError:
            return await self._reply(context, "Only number rolls supported on this channel.")
        except TokenError as e:
            return await self._reply(context, "`{name}` {message}".format(name=e.token, message=e.message))
        except ValueError as e:
            logger.warning(str(e))
            return

        result = self.roll_manager.roll(channel, user,
                                            'initiative', 1, 10,
                                            mod=mod,
                                            comment=params.comment)

        message = "Rolling initiative for {user}".format(
            user=context.message.author.mention
        ) + comment + "\r\n" + "{keycap} + [ **{mod}** ] = [ **{result}** ]".format(
            keycap=TextFormatter.keycap(result.total),
            mod=mod,
            result=mod+result.total
        )

        await self._reply(context, message)

    @command(help="Roll multiple initiatives, one per line.",
             aliases=('mi',))
    async def multiple_initiative(self, context: Context):
        if not self.valid_call(context):
            return

        message = "Rolling multiple initiatives for {user}".format(
            user=context.message.author.mention
        )
        try:
            for l in context.message.content.splitlines()[1:]:
                params = Parameters("!i " + l)
                channel = self.sheets.resolve_channel_id(params.channel) or str(context.channel.id)

                user = params.user or context.message.author.display_name
                comment = ""
                if params.comment:
                    comment = " ({comment})".format(comment=params.comment)
                mod = params.dies

                result = self.roll_manager.roll(channel, user,
                                                'initiative', 1, 10,
                                                mod=mod,
                                                comment=params.comment)

                message += "\r\n" + "{keycap} + [ **{mod}** ] = [ **{result}** ] ".format(
                    keycap=TextFormatter.keycap(result.total),
                    mod=mod,
                    result=mod + result.total
                )
                if comment:
                    message += comment
        except TypeError:
            return await self._reply(context, "Only number rolls supported on this channel.")
        except TokenError as e:
            return await self._reply(context, "`{name}` {message}".format(name=e.token, message=e.message))
        except ValueError as e:
            logger.warning(str(e))
            return

        await self._reply(context, message)

    @command(help="Displays all previous initiative rolls in the same channel, up to an hour earlier.",
             brief="Shows the initiative table",
             aliases=('table', 'it'))
    async def initiative_table(self, context: Context):
        if not self.valid_call(context):
            return

        params = Parameters(context.message.content)
        channel = self.sheets.resolve_channel_id(params.channel) or str(context.channel.id)
        init_table = self.roll_manager.history(
            channel,
            roll='initiative',
            limit=datetime.datetime.now() - datetime.timedelta(hours=1)
        )

        if not init_table:
            return await self._reply(context, "No initiative rolls found!")

        table = []
        users = set()
        for entry in init_table:
            key = str(entry.user) + '-' + str(entry.comment)
            if key not in users:
                users.add(key)
                table.append([entry.user, entry.mod, entry.result[0], entry.comment])

        table.sort(key=lambda r: r[1], reverse=True)  # Sort Mod first
        table.sort(key=lambda r: r[1]+r[2], reverse=True)  # Sort Total, so Mod breaks ties

        table = [['Player', 'Initiative', '']] + [[row[0], row[1] + row[2], row[3]] for row in table]

        message = await TextFormatter.reply_table(table)
        await self._reply(context, message)

    @command(help="Deletes all previous initiative rolls in the same channel.",
             brief="Clears the initiative table",
             aliases=('cit', 'ch'))
    async def clear_initiative_table(self, context: Context):
        if not self.valid_call(context):
            return

        params = Parameters(context.message.content)
        channel = self.sheets.resolve_channel_id(params.channel) or str(context.channel.id)
        self.roll_manager.clear_history(channel, roll='initiative')

        await self._reply(context, "Initiative table removed.")

    @command(help="Wipes the user's cache for current channel.")
    async def update(self, context: Context):
        if not self.valid_call(context):
            return

        try:
            self.sheets.purge(str(context.channel.id), context.message.author.display_name)
        except TokenError as e:
            return await self._reply(context, "`{name}` {message}".format(name=e.token, message=e.message))
        except ValueError as e:
            logger.warning(str(e))
            return

        message = "Updating sheet values for {user} on #{channel}".format(
            user=context.message.author.mention,
            channel=context.channel.name
        )

        await self._reply(context, message)

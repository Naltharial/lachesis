from grammar.lexer import Lexer
from grammar.parser import Parser


class Parameters(object):
    def __init__(self, text):
        self.lexer = Lexer()
        self.parser = Parser()

        self.text = text

        self._ast = self.parser.parse(self.lexer.tokenize(self.text))
        self._result = None

        self._args = {}
        if self._ast.args:  # TODO: Change AST to disambiguate?
            for arg in self._ast.args.exprlist:
                if arg.startswith('#'):
                    self._args['comment'] = arg.strip('#')
                if arg.startswith('@'):
                    self._args['channel'] = arg.strip('@')
                if arg.startswith('?'):
                    self._args['user'] = arg.strip('?')

    def _process(self):
        self._result = list(self._ast.result(self.resolver))

    @property
    def dies(self):
        if not self._result:
            self._process()

        return max(self._result[0], 1)

    def dies_mod(self, mod):
        if not self._result:
            self._process()

        return max(self.dies + mod, 1)

    @property
    def sides(self):
        if not self._result:
            self._process()

        if len(self._result) > 1:
            return self._result[1]

        return None

    @property
    def diff(self):
        if not self._result:
            self._process()

        if len(self._result) > 2:
            return self._result[2]

        return None

    @property
    def comment(self):
        return self._args.get('comment')

    @property
    def channel(self):
        return self._args.get('channel')

    @property
    def user(self):
        return self._args.get('user')

    def resolver(self, value):
        return value

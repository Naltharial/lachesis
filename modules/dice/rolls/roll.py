import random
from collections.abc import Sequence

from typing import List


class Roll(Sequence):
    _successes = 0
    _threshold = 0
    _botches = 0
    _extra = 0

    _botching = True
    _criticals = True
    _sorting = True
    _bounding = False

    _bounds = (1, 10,)
    _critical_range = {21 * 2**i for i in range(0, 4)}  # Critical successes set

    _result: List[int]

    # Read-only properties

    @property
    def successes(self):
        return self._successes

    @property
    def threshold(self):
        return self._threshold

    @property
    def botches(self):
        return self._botches

    @property
    def extra(self):
        return self._extra

    # Derived properties

    @property
    def critical(self):
        if self.criticals and self.total in self.critical_range:
            return True
        return False

    @property
    def total(self):
        return sum(self._result)

    # Roll settings

    @property
    def botching(self):
        return self._botching

    @botching.setter
    def botching(self, value: bool):
        if self._botching != value:
            self._botching = value
            self._refresh()

    @property
    def criticals(self):
        return self._criticals

    @criticals.setter
    def criticals(self, value: bool):
        if self._criticals != value:
            self._criticals = value
            self._refresh()

    @property
    def sorting(self):
        return self._sorting

    @sorting.setter
    def sorting(self, value: bool):
        if self._sorting != value:
            self._sorting = value
            self._refresh()

    @property
    def bounding(self):
        return self._bounding

    @bounding.setter
    def bounding(self, value: bool):
        if self._bounding != value:
            self._bounding = value
            self._refresh()

    # Roll configuration

    @property
    def bounds(self):
        return self._bounds

    @bounds.setter
    def bounds(self, value):
        self._bounding = True
        self._bounds = value
        self._refresh()

    @property
    def critical_range(self):
        return self._critical_range

    @critical_range.setter
    def critical_range(self, value):
        self._critical_range = value
        self._refresh()

    @classmethod
    def raw_roll(cls, dies, sides):
        result = [random.randint(1, sides) for x in range(dies)]

        return result

    def __init__(self, dies, sides, diff=6):
        if dies < 1 or sides < 1:
            raise ValueError("Invalid dice specification")

        self.dies = dies
        self.sides = sides
        self.diff = diff

        self._refresh()

    def __getitem__(self, i: int):
        return self._result.__getitem__(i)

    def __len__(self):
        return len(self._result)

    def __repr__(self):
        return repr(self.__dict__)

    def _refresh(self):
        self._threshold = 0
        self._botches = 0
        self._successes = 0
        self._extra = 0

        if self.bounding:
            if self.diff > self.bounds[1]:
                self._threshold = self.diff - self.bounds[1]
                self.diff = self.bounds[1]

            if self.diff < self.bounds[0]:
                self._extra = self.bounds[0] - self.diff
                self.dies += self.extra
                self.diff = self.bounds[0]

        self._result = self.raw_roll(self.dies, self.sides)
        if self.sorting:
            self._result = sorted(self._result)

        if self.botching:
            self._botches = len(list(x for x in self if x == 1))

        self._successes = len(list(x for x in self if x >= self.diff)) - self.botches - self.threshold

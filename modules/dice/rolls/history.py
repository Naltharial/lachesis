import datetime
import logging

from typing import List, Union

logger = logging.getLogger(__name__)


class Entry(object):
    user = None
    roll = None
    result = None
    mod = None
    time = None

    def __init__(self, user, roll, result, mod=None, comment=None, time=None):
        self.user = user
        self.roll = roll
        self.result = result
        self.mod = mod
        self.comment = comment
        self.time = time

        if not self.time:
            self.time = datetime.datetime.now()

    def __repr__(self):
        return "History Entry: {!s}: {!s} {!s} {!s} {!s} at {!s}".format(
            self.user,
            self.roll,
            self.result,
            self.mod,
            self.comment,
            self.time
        )


class History(object):
    def __init__(self):
        self._rolls: List[Entry] = []

    def add(self, user, roll, result, mod=None, comment=None):
        self._rolls.append(Entry(user, roll, result, mod, comment))

    def get(self, user=None, roll=None, limit: Union[int, datetime.datetime] = 10):
        # Chain filtering generators
        gen = (rt for rt in self._rolls[::-1])
        if user:
            gen = (rt for rt in gen if rt.user == user)
        if roll:
            gen = (rt for rt in gen if rt.roll == roll)

        result = []
        for rt in gen:
            if type(limit) == int:
                if len(result) == limit:
                    break
            elif type(limit) == datetime.datetime:
                if rt.time < limit:
                    break

            result.append(rt)

        return result

    def remove(self, user=None, roll=None):
        # Chain filtering generators
        gen = (rt for rt in self._rolls)
        if user:
            gen = (rt for rt in gen if rt.user != user)
        if roll:
            gen = (rt for rt in gen if rt.roll != roll)

        self._rolls = list(gen)

    def __repr__(self):
        return repr(self._rolls)

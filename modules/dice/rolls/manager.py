from typing import Dict

from modules.dice.rolls.roll import Roll
from modules.dice.rolls.history import History


class RollManager(object):
    def __init__(self):
        self._history: Dict[History] = dict()

    def roll(self, channel, user, roll, dies, sides, diff=6, mod=None, comment=None, sorting=True, bounding=False):
        if channel not in self._history:
            self._history[channel] = History()

        result = Roll(dies, sides, diff)
        result.sorting = sorting
        result.bounding = bounding

        self._history[channel].add(user, roll, result, mod, comment)

        return result

    def history(self, channel, user=None, roll=None, limit=10):
        if channel not in self._history:
            self._history[channel] = History()

        return self._history[channel].get(user, roll, limit)

    def clear_history(self, channel, user=None, roll=None):
        if channel not in self._history:
            self._history[channel] = History()

        return self._history[channel].remove(user, roll)

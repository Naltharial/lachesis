import logging

from googleapiclient.discovery import build
from httplib2 import Http
from oauth2client import file, client, tools

from grammar.expression import TokenError
from modules.dice.sheets.values import Values

logger = logging.getLogger(__name__)


class Sheets(object):
    SCOPES = 'https://www.googleapis.com/auth/spreadsheets.readonly'

    def __init__(self, config):
        self._sheets = config

        store = file.Storage('token.json')
        creds = store.get()
        if not creds or creds.invalid:
            flow = client.flow_from_clientsecrets('credentials.json', self.SCOPES)
            creds = tools.run_flow(flow, store)

        self.service = build('sheets', 'v4', http=creds.authorize(Http()))

        self._cache_user = dict()

    def _get_channel_config(self, channel):
        cc = self._sheets.get(channel)
        if not cc:
            raise ValueError("This channel is not recognized.")

        return cc

    def _get_channel_doc(self, channel, srange=None):
        ct = self._get_channel_config(channel)['id']
        if not ct:
            raise ValueError("Only number rolls supported on this channel.")

        sheet = self.service.spreadsheets()
        cd = sheet.get(spreadsheetId=ct).execute().get('sheets', [])

        if srange:
            sheet = self.service.spreadsheets()
            result = sheet.values().get(spreadsheetId=ct,
                                        range=srange).execute()
            cd = result.get('values', [])

        return cd

    def _get_user_sheet(self, channel, user):
        range = None
        cd = self._get_channel_doc(channel)
        for sheet in cd:
            if not sheet['properties'].get('hidden', False):
                author = sheet['properties'].get('title', None)
                if author:
                    at = author.lower().split()
                    ut = user.lower().split()
                    if ut[0] in at or at[0] in ut:
                        range = '{title}!{range}'.format(
                            title=author,
                            range=self._get_channel_config(channel)['range']
                        )
        if not range:
            raise TokenError(user, "- no character sheet found!")

        values = self._get_channel_doc(channel, range)
        return Values(values)

    def default_config(self, channel=None):
        if channel:
            try:
                return self._get_channel_config(channel)
            except ValueError:
                pass

        return self._get_channel_config('0')

    def resolve_channel_id(self, name):
        for cid, conf in self._sheets.items():
            if conf['channel'] == name:
                return cid

    def purge(self, channel, user):
        ct = self._get_channel_config(channel)['id']
        if not ct:
            raise ValueError("Only number rolls supported on this channel.")

        key = "{channel}+{user}".format(
            channel=channel,
            user=user
        )
        if key in self._cache_user:
            del self._cache_user[key]

    def resolver(self, channel, user):
        def lookup(name):
            logger.info("Looking up: %s on %s" % (name, channel))
            if len(name) < 3:
                raise TokenError(name, "please use at least 3 letters for matching!")

            vkey = "{channel}+{user}".format(
                channel=channel,
                user=user
            )
            values = self._cache_user.get(vkey, None)
            if not values:
                values = self._get_user_sheet(channel, user)
                self._cache_user[vkey] = values

            val = values.get(name)
            logger.info("Val: %s" % val)

            return val

        return lookup

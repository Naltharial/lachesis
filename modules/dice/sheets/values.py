import logging

from grammar.expression import TokenError

logger = logging.getLogger(__name__)


class Values(object):
    def __init__(self, values):
        self._values = values

        self._cache_value = dict()

    def order(self):
        row, col = self._locate("firstorder")[0]
        for i in range(10):
            check = self._values[row+i][col-2]
            if check:
                return int(self._values[row+i][col-1])

        return 0

    def penalty(self):  # FIXME: Generalized penalty pattern
        excessive = ("dead", "incap", "gone", "insane")

        def check(start, res=1):
            row, col = start
            for i in range(6):
                vcheck = self._values[row+8-i][col+res]
                if vcheck:
                    val = self._values[row+8-i][col+res*2]
                    val = val.lower().strip()
                    if val in excessive:
                        raise TokenError(self._values[row+8-i][col], "excessive damage!")
                    elif val == "none":
                        return 0
                    else:
                        return abs(int(val))
            return 0

        penalties = []
        phys = self._locate("bruised")
        logger.info("Physical DMG Table: %s" % phys)
        if phys:
            penalties.append(check(phys[0], res=1))
        mental = self._locate("concerned")
        logger.info("Mental DMG Table: %s" % mental)
        if mental:
            penalties.append(check(mental[0], res=-1))

        return max(penalties)

    def _locate(self, name):
        sanitize = ' -_./()'
        name = name.lower().strip()

        fuzzy = []
        for row in range(len(self._values)):
            for col in range(len(self._values[row])):
                field = self._values[row][col].strip(" -").lower()

                # Option with remove sanitized characters
                field1 = field.translate(field.maketrans('', '', sanitize))
                # Option with replaced sanitized characters to _
                field2 = field.translate(field.maketrans(sanitize, '_' * len(sanitize)))

                if field1.startswith(name) or field2.startswith(name):
                    fuzzy.append((row, col,))

        return fuzzy

    def get(self, name):
        value = self._cache_value.get(name, None)
        if not value:
            # Check for custom methods
            if hasattr(self, name):
                value = getattr(self, name)()
            else:
                matches = self._locate(name)
                for row, col in matches:
                    try:
                        val = int(self._values[row][col+1])
                    except (IndexError, ValueError) as e:
                        logger.exception("Value parse failure")
                        raise TokenError from e

                    if val is not None:
                        value = val
                        break

            if value is not None:
                self._cache_value[name] = value

        if value is None:
            raise ValueError("Value '%s' not found." % name)

        logger.info("Returning %d for %s" % (value, name))
        return value

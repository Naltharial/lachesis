import re
import abc
from typing import List, Dict

import pytz
import dateparser

from modules.intel.data.classes import make_fleet
from modules.intel.db import Station


class Parser(metaclass=abc.ABCMeta):
    class Meta:
        save = True

    station: Station
    # Iterable of tuples length 2 or 3, which describe:
    # line_startswith(str), state_name(str), [may_be_subordinate_of(Iterable(str))]
    sections = (
        ('station resources', 'resources'),
        ('station labor', 'labor'),
        ('buildings', 'buildings'),
        ('station hidden resources', 'hidden'),
        ('outposts', 'outposts'),
        ('fleets', 'fleets'),
        ('hangar', 'hangar'),
    )

    @abc.abstractmethod
    def parse(self):
        raise NotImplementedError("Implement Parser.parse")


class FullParser(Parser):
    def __init__(self, raw, user, channel_id, display_name=None):
        self.station = Station(
            user=user,
            channel=channel_id,
            display_name=display_name,
            _raw=raw
        )

        self.lines = raw.splitlines()

    def _header(self, line):
        line = line.replace('Spy Report on hex ', '')
        line = line.rstrip('.')
        target, offset = line.split('completed')

        target_rx = r"\(([-0-9]*),([-0-9]*)\)\s*([^\[]*)\s*(\[(\w*)\])*"
        matches = re.match(target_rx, target)

        self.station.hex_x = int(matches.group(1))
        self.station.hex_y = int(matches.group(2))

        self.station.name = matches.group(3).strip()
        self.station.tag = matches.group(4)

        parsed_dt = dateparser.parse(offset, settings={'RETURN_AS_TIMEZONE_AWARE': True})
        utc_dt = parsed_dt.astimezone(pytz.utc)
        utc_dt = utc_dt.replace(tzinfo=None)
        self.station.captured_at = utc_dt

    def _spy(self, line):
        spy_rx = r"([0-9]*)\."
        matches = re.search(spy_rx, line)

        self.station.rating = int(matches.group(1))

    def _resources(self, capture: List) -> Dict:
        line = capture.pop()
        lt = line.strip().split()
        return dict(zip(lt[::2], lt[1::2]))

    def _hidden(self, capture: List) -> Dict:
        return self._resources(capture)

    def _labor(self, capture: List) -> int:
        if not capture or capture[0] == "None":
            return -1

        line = capture.pop()
        lt = line.strip().split()
        return int(lt[-1])

    def _buildings(self, capture: List) -> List:
        if not capture or capture[0] == "None":
            return []

        b_list = []
        for line in capture:
            tokens = [t.strip() for t in line.split(' - ')]

            _, level = tokens[1].split()
            level = int(level.strip())

            comment = ''
            if len(tokens) > 2:
                comment = tokens[2]

            b_list.append((tokens[0], level, comment))

        return b_list

    def _outposts(self, capture: List) -> List:
        if not capture or capture[0] == "None":
            return []

        return self._buildings(capture)

    def _fleets(self, capture: List) -> List:
        if not capture or capture[0] == "None":
            return []

        f_list = []
        for line in capture:
            tokens = [t.strip() for t in line.split(maxsplit=1)]
            fleet = make_fleet(tokens[1].rstrip('s'), int(tokens[0]))
            f_list.append(fleet)

        return f_list

    def _hangar(self, capture: List) -> List:
        if not capture or capture[0] == "None":
            return []

        h_dict = {}
        for line in capture:
            hangar_rx = r"\(([\w\s]+)\)\s*([\d]+)"
            matches = re.search(hangar_rx, line)
            number = h_dict.get(matches.group(1), 0) + int(matches.group(2))
            h_dict[matches.group(1)] = number

        return list(h_dict.items())

    def _dispatch_state(self, state, capture):
        if not state:
            return

        parse_method = getattr(self, f'_{state}')
        parse_result = parse_method(capture)
        setattr(self.station, state, parse_result)

    def parse(self):
        state = None
        capture = []
        for line in self.lines:
            if line.lower().startswith('spy report'):
                self._header(line)
                continue
            if line.lower().startswith('spies'):
                self._spy(line)
                continue

            for section in self.sections:
                start = f'{section[0]}:'
                cs = section[1]
                sub = []
                if len(section) > 2:
                    sub = section[2]
                if line.lower().startswith(start) and state not in sub:
                    self._dispatch_state(state, capture)
                    state = cs
                    capture = []
                    break
            else:
                cap_line = line.strip()
                if cap_line:
                    capture.append(cap_line)

        self._dispatch_state(state, capture)


class NoParser(FullParser):
    class Meta:
        save = False

    sections = ()

    def parse(self):
        self.lines.pop()  # Throw away the partial notice
        super(NoParser, self).parse()


class PartialParser(FullParser):
    def _buildings(self, capture: List) -> List:
        if not capture or capture[0] == "None":
            return []

        b_list = []
        for line in capture:
            tokens = [t.strip() for t in line.split(' - ')]

            level = -1
            comment = ''

            b_list.append((tokens[0], level, comment))

        return b_list

    def _fleets(self, capture: List) -> List:
        if not capture or capture[0] == "None":
            return []

        f_list = []
        for line in capture:
            fleet = make_fleet(line.strip().rstrip('s'), -1)
            f_list.append(fleet)

        return f_list

    def parse(self):
        self.lines.pop()  # Throw away the partial notice
        super(PartialParser, self).parse()


class TianMixin(object):
    def _cards(self, capture: List) -> List:
        if not capture or capture[0] == "None":
            return []

        line = capture.pop()
        lt = line.rstrip('.').strip().split(', ')

        b_list = []
        for card in lt:
            tt, name = card.strip().split(maxsplit=1)
            tt = re.sub('[()]', ' ', tt)
            fun, cid = tt.strip().split()

            b_list.append((name, cid))

        return b_list


class TianPartialParser(PartialParser, TianMixin):
    sections = (
        ('station resources', 'resources'),
        ('cards', 'cards'),
        ('station labor', 'labor'),
        ('buildings', 'buildings'),
        ('station hidden resources', 'hidden'),
        ('outposts', 'outposts'),
        ('fleets', 'fleets'),
        ('hangar', 'hangar'),
    )


class TianFullParser(FullParser, TianMixin):
    sections = (
        ('station resources', 'resources'),
        ('cards', 'cards', ('fleets',)),
        ('station labor', 'labor'),
        ('buildings', 'buildings'),
        ('construction queues', None),
        ('building construction queue', 'build_queue'),
        ('fleet construction queue', 'fleet_queue'),
        ('station hidden resources', 'hidden'),
        ('outposts', 'outposts'),
        ('fleets', 'fleets'),
        ('hangar', 'hangar'),
    )

    def _fleets(self, capture: List) -> List:
        if not capture or capture[0] == "None":
            return []

        if 'this station' in capture[0]:
            supply_line = capture.pop(0)
            supply_rx = r"([\d]+)"
            matches = re.match(supply_rx, supply_line)

            self.station.supplies = int(matches.group(1))

        f_list = []
        for line in capture:
            if line.startswith('From'):
                continue
            elif line.startswith('Cards:'):
                line = line.replace('Cards: ', '')
                f_list[-1] = make_fleet(f_list[-1].hull, f_list[-1].number, level=1, cards=self._cards([line]))
            else:
                no_cards = None
                if 'cards' in line:
                    line, no_cards = [t.strip() for t in line.split('-')]

                tokens = [t.strip() for t in line.split(maxsplit=1)]
                fleet = make_fleet(tokens[1].rstrip('s'), int(tokens[0]))
                if no_cards:
                    fleet.cards = []

                f_list.append(fleet)

        return f_list

    def _build_queue(self, capture: List) -> List:
        if not capture or capture[0] == "Empty":
            return []

        if capture[0].startswith('Upgrading'):
            capture.pop(0)

        return self._buildings(capture)

    def _fleet_queue(self, capture: List) -> List:
        if not capture or capture[0] == "Empty":
            return []

        progress = None
        if capture[0].startswith('Progress'):
            prog_line = capture.pop(0)
            _, perc_str = prog_line.strip().split(maxsplit=1)
            progress = float(perc_str.strip('%')) / 100.0

        h_dict = {}
        for line in capture:
            tokens = [t.strip() for t in line.split(maxsplit=1)]
            mod_num = int(tokens[0])
            name = tokens[1].strip().rstrip('s')
            if progress:
                mod_num -= int(mod_num * progress)
                progress = None
            h_dict[name] = h_dict.get(name, 0) + mod_num

        return list(h_dict.items())

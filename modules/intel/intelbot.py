import io
import datetime
import logging
from typing import Optional, List

import pytz
import arrow
import discord
from discord.ext.commands import Context, Cog, command
from playhouse.shortcuts import model_to_dict

from modules import hex_map
from modules.base import BaseModule
from modules.intel.db import sqlite_db, Station
from modules.intel.embed import StationEmbed
from modules.intel.parsers import TianFullParser, TianPartialParser, FullParser, PartialParser, NoParser

logger = logging.getLogger(__name__)

sqlite_db.connect()
sqlite_db.create_tables([Station])


class Intelbot(BaseModule):
    @classmethod
    def _make_parser(cls, content: str, channel: discord.GroupChannel, user: discord.User = None):
        display_name = None
        if user:
            display_name = user.display_name

        if 'Cards' in content:
            if 'unknown' in content:
                parser = TianPartialParser(content, user, channel.id, display_name=display_name)
            else:
                parser = TianFullParser(content, user, channel.id, display_name=display_name)
        else:
            if 'unable to gather any intel' in content:
                parser = NoParser(content, user, channel.id, display_name=display_name)
            elif 'unknown' in content:
                parser = PartialParser(content, user, channel.id, display_name=display_name)
            else:
                parser = FullParser(content, user, channel.id, display_name=display_name)

        return parser

    @classmethod
    async def _get_embed(cls, content: str, message: discord.Message) -> Optional[StationEmbed]:
        if not content.startswith('Spy Report'):
            return

        parser = cls._make_parser(content, message.channel, message.author)
        parser.parse()
        if parser.Meta.save:
            Station.replace(model_to_dict(parser.station)).execute()
            sqlite_db.commit()

        embed = StationEmbed(parser.station)

        return embed

    def _get_stations(self, user, hexes) -> List[Station]:
        parsed, comment = hex_map.parse_hexes(hexes)
        if len(parsed) != 1:
            raise ValueError("Invalid hex specifier for a single hex")

        existing = []
        hex_x, hex_y = parsed.pop()
        channels = self.get_user_channels(user)
        if not channels:
            return []
        elif len(channels) == 1:
            st = Station.latest(channels.pop(), hex_x, hex_y)
            if st:
                existing.append(st)
        else:
            com_cid = comment.strip()
            if com_cid:
                try:
                    com_cid = int(com_cid)
                except ValueError:
                    com_cid = None

            if com_cid:
                st = Station.latest(com_cid, hex_x, hex_y)
                if st:
                    existing.append(st)
            else:
                for cid in channels:
                    st = Station.latest(cid, hex_x, hex_y)
                    if st:
                        existing.append(st)

        return existing

    @Cog.listener()
    async def on_message(self, message: discord.Message):
        if not self.valid_call(message):
            return

        if message.author.id == self.bot.user.id:
            return

        parsed = False
        content = message.content.strip()
        embed = await self._get_embed(content, message)
        if embed:
            await message.channel.send(embed=embed.get())
            parsed = True

        for attachment in message.attachments:
            if attachment.filename.endswith('.txt'):
                byte_content = await attachment.read()
                content = byte_content.decode(encoding='utf-8')
                embed = await self._get_embed(content, message)
                if embed:
                    await message.channel.send(embed=embed.get())
                    parsed = True

        if parsed:
            await message.delete()

    @command(help="(DM) Fetches last parsed report")
    async def report(self, context: Context,
                     *, hexes: str):
        if not self.valid_call(context, private=True):
            return

        try:
            existing = self._get_stations(context.author, hexes)
        except ValueError as e:
            await context.channel.send(str(e))
            return

        if not existing:
            await context.channel.send("No parsed report found")
        else:
            if len(existing) == 1:
                embed = StationEmbed(existing.pop())
                await context.channel.send(embed=embed.get())
            else:
                message = ("Multiple options found on different servers - "
                           "please specify channel id at the end of the command:")
                for st in existing:
                    message += f"\n   {st.channel} ({st.name})"
                await context.channel.send(message)

    @command(help="(DM) Fetches last raw report")
    async def spy(self, context: Context,
                  *, hexes: str):
        if not self.valid_call(context, private=True):
            return

        try:
            existing = self._get_stations(context.author, hexes)
        except ValueError as e:
            await context.channel.send(str(e))
            return

        if not existing:
            await context.channel.send("No raw report found")
        else:
            if len(existing) == 1:
                st = existing.pop()
                captured_at = arrow.Arrow.fromdatetime(st.captured_at, tzinfo=pytz.utc).format('YYYY-MM-DD_HH-mm')
                raw_file = discord.File(io.StringIO(st._raw), filename=f"{st.hex_x}x{st.hex_y}_{captured_at}.txt")
                await context.channel.send(f"Last raw report for {st.name} at ({st.hex_x} x {st.hex_y}):", file=raw_file)
            else:
                message = ("Multiple options found on different servers - "
                           "please specify channel id at the end of the command:")
                for st in existing:
                    message += f"\n   {st.channel} ({st.name})"
                await context.channel.send(message)

    @command(help="(DM) Calculates travel time")
    async def travel(self, context: Context,
                     *, hexes: str):
        if not self.valid_call(context, private=True):
            return

        def _parse_travel(distance, speed):
            result = datetime.timedelta(hours=distance/speed)

            days = result.days
            minutes, seconds = divmod(result.seconds, 60)
            hours, minutes = divmod(minutes, 60)

            day_str = ""
            if days:
                day_str = f"{days} days, "
            return f"at speed {speed}, it takes {day_str}{hours:2}:{minutes:02}:{seconds:02} to traverse"

        parsed, comment = hex_map.parse_hexes(hexes)
        if len(parsed) != 2:
            await context.channel.send("Invalid hex specifier for two hexes")
            return

        hexa = parsed.pop()
        hexb = parsed.pop()

        speed = 0
        try:
            speed = int(comment)
        except ValueError:
            pass

        distance = hex_map.hex_distance(*hexa, *hexb)

        travel_msg = f"Distance between **{hexa[0]},{hexa[1]}** and **{hexb[0]},{hexb[1]}** is **{distance}** hexes."
        if speed:
            travel_msg += f"\n   {_parse_travel(distance, speed)}"
        else:
            for speed in [6, 8, 12]:
                travel_msg += f"\n   {_parse_travel(distance, speed)}"

        await context.channel.send(travel_msg)

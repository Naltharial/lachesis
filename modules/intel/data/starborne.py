import pathlib
import commentjson as json
from decimal import Decimal
from typing import List, Dict

MAX_MODIFIERS = {
    'LightAssault': {
        'UNIT_FIREPOWER': {
            'Modifiers': [],
            'Multipliers': []
        },
        'UNIT_HP': {
            'Modifiers': [],
            'Multipliers': []
        }
    },
    'LightDefense': {
        'UNIT_FIREPOWER': {
            'Modifiers': [
                20,  # Frontier Legion
            ],
            'Multipliers': []
        },
        'UNIT_HP': {
            'Modifiers': [],
            'Multipliers': []
        }
    },
    'HeavyAssault': {
        'UNIT_FIREPOWER': {
            'Modifiers': [],
            'Multipliers': []
        },
        'UNIT_HP': {
            'Modifiers': [],
            'Multipliers': []
        }
    },
    'HeavyDefense': {
        'UNIT_FIREPOWER': {
            'Modifiers': [
                80,  # MPL
            ],
            'Multipliers': []
        },
        'UNIT_HP': {
            'Modifiers': [
                200,  # MPL
            ],
            'Multipliers': []
        }
    },
    'Dreadnought': {
        'UNIT_FIREPOWER': {
            'Modifiers': [],
            'Multipliers': []
        },
        'UNIT_HP': {
            'Modifiers': [],
            'Multipliers': []
        }
    },
    'Carrier': {
        'UNIT_FIREPOWER': {
            'Modifiers': [],
            'Multipliers': []
        },
        'UNIT_HP': {
            'Modifiers': [],
            'Multipliers': []
        }
    },
    'Bomber': {
        'UNIT_FIREPOWER': {
            'Modifiers': [],
            'Multipliers': []
        },
        'UNIT_HP': {
            'Modifiers': [],
            'Multipliers': []
        }
    },
    'Scout': {
        'UNIT_FIREPOWER': {
            'Modifiers': [],
            'Multipliers': []
        },
        'UNIT_HP': {
            'Modifiers': [],
            'Multipliers': []
        }
    },
    'HeavyScout': {
        'UNIT_FIREPOWER': {
            'Modifiers': [],
            'Multipliers': []
        },
        'UNIT_HP': {
            'Modifiers': [],
            'Multipliers': []
        }
    },
    'TroopCarrier': {
        'UNIT_FIREPOWER': {
            'Modifiers': [],
            'Multipliers': []
        },
        'UNIT_HP': {
            'Modifiers': [],
            'Multipliers': []
        }
    }
}

EXTRA_CATEGORIES = {
    'LightAssault': ['LightCombatShips'],
    'LightDefense': ['LightCombatShips'],
    'Light': ['SmallShips'],
    'Heavy': ['LargeShips']
}

ships = {}
buildings = {}
outposts = {}
cards = {}


def _parse_modifier_array(mod_array):
    stats = []
    for stat in mod_array:
        if isinstance(stat, str):
            stat = stat.replace(',', '')

        stats.append(round(Decimal(stat), 2))

    return stats


def parse_ships():
    class_data = json.load(open(pathlib.Path(__file__).parent / 'LOCALIZATION_SHIPS.json', 'rt', encoding='utf-8-sig'))
    ship_data = json.load(open(pathlib.Path(__file__).parent / 'UNIT_DEFINITIONS.json', 'rt', encoding='utf-8-sig'))

    for block in class_data['ShipClasses']:
        categories = EXTRA_CATEGORIES.get(block['Type'], [])
        categories.append(block['ClassSize'])
        categories.append(f"{block['ClassSize']}Ships")
        categories += EXTRA_CATEGORIES.get(block['ClassSize'], [])

        ships[block['Type']] = {
            'Hull': block['ClassName'],
            'Class': block['ClassType'],
            'Size': block['ClassSize'],
            'Categories': categories,
            'Progression': {}
        }

    for root_key in ship_data:
        if root_key == 'StatProgression':
            for block in ship_data[root_key]:
                ships[block['Target']]['Progression'][block['TargetAttribute']] = {
                    'Type': block['Type'],
                    'Modifier': list(block['ModifierArray'])
                }
        elif root_key == 'Units':
            for block in ship_data[root_key]:
                name = block['Name']
                for _, definition in ships.items():
                    if name == definition['Hull']:
                        definition['Stats'] = dict(block['Stats'])
                        break


def parse_buildings():
    building_data = json.load(open(pathlib.Path(__file__).parent / 'BUILDING_DEFINITIONS.json', 'rt', encoding='utf-8-sig'))

    def _parse_block(block, tags=None):
        building = {
            'Tags': tags
        }
        for stat_block in block['Stats']:
            if stat_block['TargetAttribute'] == 'STAT_B_HP':
                building['STAT_B_HP'] = _parse_modifier_array(stat_block['ModifierArray'])
        building['Effects'] = block.get('Effects', [])

        buildings[block['Name']] = building

    for block in building_data['SuperStructureBuildings']:
        _parse_block(block, ['SuperStructure'])

    for block in building_data['FobBuildings']:
        _parse_block(block, ['ForwardBase'])

    for block in building_data['AllianceStationBuildings']:
        _parse_block(block, ['AllianceStation'])

    for block in building_data['BuildingTypes']:
        _parse_block(block, ['Normal'])


def parse_outposts():
    outpost_data = json.load(open(pathlib.Path(__file__).parent / 'OUTPOST_DEFINITIONS.json', 'rt', encoding='utf-8-sig'))

    for block in outpost_data['Definitions']:
        outposts[block['Name']] = {}
        for stat_block in block['Stats']:
            if stat_block['TargetAttribute'] == 'OP_HITPOINTS':
                outposts[block['Name']]['OP_HITPOINTS'] = _parse_modifier_array(stat_block['ModifierArray'])


def parse_cards():
    card_data = json.load(open(pathlib.Path(__file__).parent / 'CARDS.json', 'rt', encoding='utf-8-sig'))

    for block in card_data['FleetCards']:
        if block.get('Effects'):
            name = block['Name']
            cards[name] = {
                'id': int(block['ID']),
                'name': name,
                'type': 'fleet',
                'effects': block['Effects'],
            }

    for block in card_data['StationCards']:
        if block.get('Effects'):
            name = block['Name']
            cards[name] = {
                'id': int(block['ID']),
                'name': name,
                'type': 'station',
                'effects': block['Effects'],
            }

    for block in card_data['OtherCards']:
        if block.get('Effects'):
            name = block['Name']
            cards[name] = {
                'id': int(block['ID']),
                'name': name,
                'type': 'station',
                'effects': block['Effects'],
            }


def parse_block(block: Dict, modifiers: List, multipliers: List, level: int = 0):
    if block["Type"] == "Modifier":
        if block.get('UseArray'):
            modifiers.append(float(block['ModifierArray'][level]))
        else:
            modifiers.append(float(block['Modifier']))
    elif block["Type"] == "Multiplier":
        if block.get('UseArray'):
            multipliers.append(float(block['ModifierArray'][level]))
        else:
            multipliers.append(float(block['Modifier']))


def parse_stat(origin: Dict, stat: str, modifiers: List, multipliers: List, level: int = 1):
    if stat in origin:
        parse_block(origin[stat], modifiers, multipliers, level)


def get_max_mods(targets, stat):
    mods = {
        stat: {
            'Modifiers': [],
            'Multipliers': []
        }
    }

    for target in targets:
        if target in MAX_MODIFIERS:
            if stat in MAX_MODIFIERS[target]:
                mods[stat]['Modifiers'] += list(MAX_MODIFIERS[target]['Modifiers'])
                mods[stat]['Multipliers'] += list(MAX_MODIFIERS[target]['Multipliers'])
    return mods


parse_ships()
parse_buildings()
parse_outposts()
parse_cards()

from dataclasses import dataclass, field
from typing import Optional, Any, Dict, List, Tuple

from modules.intel.data import starborne


@dataclass
class Card(object):
    name: str
    effects: List[Dict[str, Any]]

    def get_effects(self, stat: str, targets: List, modifiers: List, multipliers: List,
                    level: int = 1):
        for block in self.effects:
            if block["TargetAttribute"] == stat:
                if "Target" in block and block["Target"] not in targets:
                    continue

                starborne.parse_block(block, modifiers, multipliers, level - 1)


@dataclass
class Fleet(object):
    hull: str
    size: str

    number: int

    firepower: int
    hitpoints: int
    bombing: int

    categories: List[str]

    level: int = 1

    cards: List[Card] = field(default_factory=list)
    progression: Dict = field(default_factory=dict)
    modifiers: Dict[str, Dict[str, List]] = field(default_factory=dict)  # See self.max_fp / self.max_hp

    def get_building_effects(self, stat: str, modifiers: List, multipliers: List,
                             level: int = 1):
        for name in starborne.buildings:
            for block in starborne.buildings[name]['Effects']:
                if block['TargetAttribute'] == stat and block.get('Destination') == 'Fleets':
                    if block.get('Conditions') or block['Target'] not in self.categories:
                        continue

                    starborne.parse_block(block, modifiers, multipliers, level)

    def level_stat(self, stat, base=0):
        stat_mod = []
        stat_mul = []

        starborne.parse_stat(self.progression, stat, stat_mod, stat_mul, self.level)

        stat_base = base + sum(stat_mod)
        mod = 1 + sum(stat_mul)

        return stat_base * mod

    def total_stat(self, stat, base=0):
        stat_mod = []
        stat_mul = []

        for card in self.cards:
            card.get_effects(stat, self.categories, stat_mod, stat_mul, self.level)

        if stat in self.modifiers:
            stat_mod += self.modifiers[stat]['Modifiers']
            stat_mul += self.modifiers[stat]['Multipliers']

        stat_base = self.level_stat(stat, base) + sum(stat_mod)
        mod = 1 + sum(stat_mul)
        sum_stat = self.number * stat_base * mod

        return sum_stat

    def merge_mods(self, mods):
        for stat in self.modifiers:
            if stat not in mods:
                mods[stat] = {
                    'Modifiers': [],
                    'Multipliers': []
                }

            mods[stat]['Modifiers'] += self.modifiers[stat]['Modifiers']
            mods[stat]['Multipliers'] += self.modifiers[stat]['Multipliers']

        return mods

    @property
    def total_fp(self):
        return self.total_stat('UNIT_FIREPOWER', self.firepower)

    @property
    def total_hp(self):
        return self.total_stat('UNIT_HP', self.hitpoints)

    @property
    def max_fp(self):
        cards_max = [
            ('Laser Cannon', 0),
            ('Maser Cannon', 0),
            ('Target Computer', 0),
            ('Fleet Cadet', 0),

            ('Mumei Nakamura', 0)
        ]

        mod_max = starborne.get_max_mods(self.categories, 'UNIT_FIREPOWER')
        mod_max['UNIT_FIREPOWER']['Multipliers'] += [
            0.06,  # Policies - Overkill
        ]

        self.get_building_effects('UNIT_FIREPOWER',
                                  mod_max['UNIT_FIREPOWER']['Modifiers'],
                                  mod_max['UNIT_FIREPOWER']['Multipliers'],
                                  -1)

        self.merge_mods(mod_max)

        fleet_max = make_fleet(self.hull, self.number, 4, cards_max, mod_max)

        return fleet_max.total_fp

    @property
    def max_hp(self):
        cards_max = [
            ('Rhodium Coating', 0),
            ('Graphene Armor', 0),
            ('Target Computer', 0),
            ('Fleet Cadet', 0),

            ('Mumei Nakamura', 0)
        ]

        mod_max = starborne.get_max_mods(self.categories, 'UNIT_HP')
        mod_max['UNIT_HP']['Multipliers'] += [
            0.06,  # Policies - Elite Navy
            0.14,  # Policies - Defense Doctrine
        ]

        self.get_building_effects('UNIT_HP',
                                  mod_max['UNIT_HP']['Modifiers'],
                                  mod_max['UNIT_HP']['Multipliers'],
                                  -1)

        self.merge_mods(mod_max)

        fleet_max = make_fleet(self.hull, self.number, 4, cards_max, mod_max)

        return fleet_max.total_hp


@dataclass
class Wing(object):
    fleets: List[Fleet]

    buildings: List[Tuple]

    def __post_init__(self):
        fleets = []
        for fleet in self.fleets:
            mod_max = {}
            for stat in 'UNIT_FIREPOWER', 'UNIT_HP', 'UNIT_BOMB':
                mod_max[stat] = {
                        'Modifiers': [],
                        'Multipliers': []
                    }

                for name, level, _ in self.buildings:
                    for block in starborne.buildings[name]['Effects']:
                        if block['TargetAttribute'] == stat and block.get('Destination') == 'FleetsInRange':
                            if block.get('Conditions') or block['Target'] not in fleet.categories:
                                continue

                            starborne.parse_block(block,
                                                  mod_max[stat]['Modifiers'],
                                                  mod_max[stat]['Multipliers'],
                                                  level - 1)

            fleet.merge_mods(mod_max)
            fleet_max = make_fleet(fleet.hull, fleet.number, 4, [], mod_max)
            fleet_max.cards = fleet.cards

            fleets.append(fleet_max)

        self.fleets = fleets

    @property
    def total_fp(self):
        return sum([fleet.total_fp for fleet in self.fleets])

    @property
    def total_hp(self):
        return sum([fleet.total_hp for fleet in self.fleets])

    @property
    def max_fp(self):
        return sum([fleet.max_fp for fleet in self.fleets])

    @property
    def max_hp(self):
        return sum([fleet.max_hp for fleet in self.fleets])

    def fleet_totals(self):
        f_dict = {}
        for fleet in self.fleets:
            f_calc = f_dict.get(fleet.hull, {})
            f_fleets = f_calc.get('fleets', 0) + 1
            f_number = f_calc.get('number', 0) + fleet.number
            f_fp = f_calc.get('firepower', 0) + fleet.total_fp
            f_hp = f_calc.get('hitpoints', 0) + fleet.total_hp

            f_dict[fleet.hull] = {
                'fleets': f_fleets,
                'number': f_number,
                'firepower': f_fp,
                'hitpoints': f_hp,
            }

        return list(f_dict.items())


@dataclass
class Buildings(object):
    building_effect = 'Barricades'
    outpost_effect = 'Outpost Management Services'

    outposts: List[Tuple]
    buildings: List[Tuple]

    @property
    def total_buildings(self):
        hps = 0
        for name, level, _ in self.buildings:
            hps += self.building_hp(name=name, level=level)

        return hps

    @property
    def total_outposts(self):
        hps = 0
        for name, level, _ in self.outposts:
            hps += self.outpost_hp(name=name, level=level)

        return hps

    def building_hp(self, name: str, level: int) -> float:
        if level < 0:
            return -1.0

        total = float(starborne.buildings[name]['STAT_B_HP'][level-1])
        for name, level, _ in self.buildings:
            if name == self.building_effect:
                for stat_block in starborne.buildings[name]['Effects']:
                    if stat_block['TargetAttribute'] == 'STAT_B_HP':
                        total *= 1 + stat_block['ModifierArray'][level-1]

        return total

    def outpost_hp(self, name: str, level: int) -> float:
        if level < 0:
            return -1.0

        total = float(starborne.outposts[name]['OP_HITPOINTS'][level-1])
        for name, level, _ in self.buildings:
            if name == self.outpost_effect:
                for stat_block in starborne.buildings[name]['Effects']:
                    if stat_block['TargetAttribute'] == 'OP_HITPOINTS':
                        total *= 1 + stat_block['ModifierArray'][level-1]

        return total


def make_fleet(hull: str, number: int, level: int = 1,
               cards: Optional[List[Tuple]] = None, modifiers: Optional[Dict[str, Dict]] = None):
    block = starborne.ships.get(hull.replace(' ', ''))  # Heavy Scout ...
    if not block:
        for _, definition in starborne.ships.items():
            if hull == definition['Hull']:
                block = definition
                break

    if not block:
        raise ValueError(f"No hull {hull} defined")

    if not cards:
        cards = []

    if not modifiers:
        modifiers = {}

    return Fleet(
        hull=block['Hull'],
        number=number,

        level=level,
        size=block['Size'],
        categories=block['Categories'],

        firepower=block['Stats'].get('UNIT_FIREPOWER', 0),
        hitpoints=block['Stats'].get('UNIT_HP', 0),
        bombing=block['Stats'].get('UNIT_BOMB', 0),

        cards=[Card(
            name=c,
            effects=starborne.cards[c]['effects']
        ) for c, cid in cards],
        progression={
            'firepower': block['Progression'].get('UNIT_FIREPOWER'),
            'hitpoints': block['Progression'].get('UNIT_HP')
        },
        modifiers=modifiers
    )

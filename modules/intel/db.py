import json
import functools

from peewee import Model, SqliteDatabase, CompositeKey, DoesNotExist
from peewee import TextField, IntegerField, DateTimeField
from playhouse.sqlite_ext import JSONField

from modules.intel.data.classes import Fleet, Card, Wing, Buildings, make_fleet

sqlite_db = SqliteDatabase('intelbot.db')


class CardEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, Card):
            return {
                'name': obj.name,
                'effects': obj.effects
            }

        return json.JSONEncoder.default(self, obj)

    @classmethod
    def decode(cls, decoding):
        if 'name' not in decoding:
            return decoding

        return Card(**decoding)


class FleetEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, Fleet):
            return {
                'hull': obj.hull,
                'number': obj.number,
                'cards': [(c.name, c.effects) for c in obj.cards]
            }

        return json.JSONEncoder.default(self, obj)

    @classmethod
    def decode(cls, decoding):
        if 'hull' not in decoding:
            return decoding

        return make_fleet(**decoding)


def empty_resource():
    return {'Metal': 0, 'Gas': 0, 'Crystal': 0}


class BaseModel(Model):
    class Meta:
        database = sqlite_db


class Station(BaseModel):
    class Meta:
        primary_key = CompositeKey('channel', 'hex_x', 'hex_y')

    user = TextField()
    display_name = TextField(null=True)

    channel = IntegerField()

    name = TextField()
    rating = IntegerField()
    tag = TextField(null=True)
    supplies = IntegerField(null=True)

    captured_at = DateTimeField()

    hex_x = IntegerField()
    hex_y = IntegerField()

    labor = IntegerField(null=True)

    resources = JSONField(default=empty_resource)
    hidden = JSONField(default=empty_resource)

    cards = JSONField(null=True,
                      json_dumps=functools.partial(json.dumps, cls=CardEncoder),
                      json_loads=functools.partial(json.loads, object_hook=CardEncoder.decode))

    fleets = JSONField(default=list,
                       json_dumps=functools.partial(json.dumps, cls=FleetEncoder),
                       json_loads=functools.partial(json.loads, object_hook=FleetEncoder.decode))

    buildings = JSONField(null=list)
    outposts = JSONField(null=list)
    hangar = JSONField(null=list)

    build_queue = JSONField(null=True)
    fleet_queue = JSONField(null=True)

    _raw = TextField()

    def __init__(self, *args, **kwargs):
        super(Station, self).__init__(*args, **kwargs)

        self._wing_data = None
        self._building_data = None

    @property
    def building_data(self):
        if not self._building_data:
            self._building_data = Buildings(buildings=self.buildings, outposts=self.outposts)

        return self._building_data

    @property
    def wing_data(self):
        if not self._wing_data:
            self._wing_data = Wing(fleets=self.fleets, buildings=self.buildings)

        return self._wing_data

    @property
    def total_buildings(self):
        return self.building_data.total_buildings

    @property
    def total_outposts(self):
        return self.building_data.total_outposts

    @property
    def wing(self):
        return self.wing_data

    def building_hp(self, name, level):
        return self.building_data.building_hp(name=name, level=level)

    def outpost_hp(self, name, level):
        return self.building_data.outpost_hp(name=name, level=level)

    @classmethod
    def latest(cls, channel: int, hex_x: int, hex_y: int):
        result = None
        try:
            result = cls.select().where(
                cls.channel == channel,
                cls.hex_x == hex_x,
                cls.hex_y == hex_y
            ).order_by(cls.captured_at.desc()).get()
        except DoesNotExist:
            pass

        return result

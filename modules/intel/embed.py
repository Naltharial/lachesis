import pytz
import arrow
import discord
from decimal import Decimal, getcontext

from modules.intel.db import Station


class StationEmbed(object):
    station: Station

    @property
    def ago(self):
        at = arrow.Arrow.fromdatetime(self.station.captured_at, tzinfo=pytz.utc)
        return at.humanize()

    @classmethod
    def human_format(cls, number, precision=2, suffixes=None):
        if suffixes is None:
            suffixes = ('', ' K', ' M', ' G', ' T', ' P')

        suffix = ''
        getcontext().prec = 8
        number = Decimal(number)
        if suffixes:
            m = sum([round(number/Decimal(1000.0**x)) >= 1 for x in range(1, len(suffixes))])
            number = number / Decimal(1000.0**m)
            suffix = suffixes[m]

        if number == number.to_integral_value():
            precision = 0
        else:
            one_s = number * 10
            if one_s == one_s.to_integral_value():
                precision = 1

        return f'{number:.{precision}f}{suffix}'

    def __init__(self, station: Station):
        self.station = station

    def _buildings(self) -> str:
        if not self.station.buildings:
            return "None\n"

        building_text = ""
        for building, level, comments in self.station.buildings:
            hp = ""
            if level == -1:
                level = "??"
            else:
                hp = self.station.building_hp(building, level)
                hp = f", {self.human_format(hp, suffixes=[])} HP"
            if comments:
                comments = ", " + comments
            building_text += f"{building} **{level}**{hp}{comments}\n"

        if self.station.total_buildings < 0:
            building_text += f"\nTotal hitpoints: **??**\n"
        else:
            hp = self.human_format(self.station.total_buildings, suffixes=[])
            building_text += f"\nTotal hitpoints: **{hp}**\n"

        return building_text

    def _outposts(self) -> str:
        if not self.station.outposts:
            return "None\n"

        outpost_text = ""
        for outpost, level, comments in self.station.outposts:
            hp = ""
            if level == -1:
                level = "??"
            else:
                hp = self.station.outpost_hp(outpost, level)
                hp = f", {self.human_format(hp, suffixes=[])} HP"
            if comments:
                comments = ", " + comments
            outpost_text += f"{outpost} **{level}**{hp}{comments}\n"

        if self.station.total_outposts < 0:
            outpost_text += f"\nTotal hitpoints: **??**\n"
        else:
            hp = self.human_format(self.station.total_outposts, suffixes=[])
            outpost_text += f"\nTotal hitpoints: **{hp}**\n"

        return outpost_text

    def _resources(self) -> str:
        return ', '.join([f'{r}: {n}' for r, n in self.station.resources.items()])

    def _build_queue(self) -> str:
        if not self.station.build_queue:
            return "Empty\n"

        building_queue_text = ""
        for building_type, level, _ in self.station.build_queue:
            building_queue_text += f"Level {level} {building_type}\n"

        return building_queue_text

    def _fleet_queue(self) -> str:
        if not self.station.fleet_queue:
            return "Empty\n"

        fleet_queue_text = ""
        for ship_type, number in self.station.fleet_queue:
            fleet_queue_text += f"{number} {ship_type}\n"

        return fleet_queue_text

    def _fleets(self) -> str:
        if not self.station.fleets:
            return "Empty\n"

        fleet_text = ""
        for fleet, data in self.station.wing.fleet_totals():
            if data['number'] < 0:
                fleet_text += f"{data['fleets']:2}x {fleet}: ??\n"
            else:
                fp = self.human_format(data['firepower'])
                hp = self.human_format(data['hitpoints'])
                fleet_text += (f"{data['fleets']:2}x {fleet}: {data['number']}, "
                               f"firepower: {fp}, "
                               f"hitpoints: {hp}\n")

        tfp = self.station.wing.total_fp
        thp = self.station.wing.total_hp
        if tfp > 0 and thp > 0:
            fp = self.human_format(tfp)
            hp = self.human_format(thp)
            fleet_text += (f"\nTotal firepower: **{fp}**, "
                           f"hitpoints: **{hp}**\n")

            fleet_text += "\n ... adding possible cards, buildings, policies ..."

            fp = self.human_format(self.station.wing.max_fp)
            hp = self.human_format(self.station.wing.max_hp)
            fleet_text += (f"\nMaximum defender firepower: **{fp}**, "
                           f"hitpoints: **{hp}**\n")

        return fleet_text

    def _hangar(self) -> str:
        if not self.station.hangar:
            return "Empty\n"

        hangar_text = ""
        for hangar, number in self.station.hangar:
            hangar_text += f"{hangar}: {number}\n"

        return hangar_text

    def _stats(self) -> str:
        stats_text = f"Labor: {self.station.labor}\nSpy defence: {self.station.rating}"
        return stats_text

    def get(self):
        tag = ''
        supplies = ''
        name = self.station.display_name or self.station.user
        if self.station.tag:
            tag = f" {self.station.tag}"
        if self.station.supplies:
            supplies = f"This station supplies {self.station.supplies} fleets\n\n"
        embed = discord.Embed(
            title=f"Report on {self.station.name}{tag}, *{self.ago}*",
            description=f"{supplies}Reported by {name}\n`/goto {self.station.hex_x} {self.station.hex_y}`"
        )

        embed.add_field(name="**Stats**", value=self._stats(), inline=False)

        embed.add_field(name="**Buildings**", value=self._buildings(), inline=True)
        embed.add_field(name="**Outposts**", value=self._outposts(), inline=True)

        embed.add_field(name="**Resources**", value=self._resources(), inline=False)

        if self.station.fleet_queue is not None:
            embed.add_field(name="**Building queue**", value=self._build_queue(), inline=True)
            embed.add_field(name="**Fleet queue**", value=self._fleet_queue(), inline=True)

        embed.add_field(name="**Total Fleets**", value=self._fleets(), inline=False)

        embed.add_field(name="**Total Ships in Hangar**", value=self._hangar(), inline=False)

        footer_text = (f"DM me !report {self.station.hex_x} {self.station.hex_y} to get the latest report,"
                       f" or !spy {self.station.hex_x} {self.station.hex_y} for the raw text")
        embed.set_footer(text=footer_text)

        return embed

import re
from typing import Tuple


def parse_hexes(hexes: str) -> Tuple:
    hex_rx = r"\[?([-0-9]+)[ ,]([-0-9]+)\]?"
    results = []
    remain = ""
    while hexes:
        match = re.search(hex_rx, hexes)
        if match:
            results.append((int(match.group(1)), int(match.group(2))))
            hexes = hexes[match.end():]
        else:
            remain = hexes.strip()
            hexes = ""

    return results, remain


def hex_distance(hexa_x, hexa_y, hexb_x, hexb_y):
    return int((abs(hexa_x - hexb_x)
            + abs(hexa_x + hexa_y - hexb_x - hexb_y)
            + abs(hexa_y - hexb_y)) / 2)
